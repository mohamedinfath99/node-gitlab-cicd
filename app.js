import express from "express";

const app = express ();

app.get('/', (req, res) => {
    res.send("Learning CI/CD using Node.js")
})

const port = 5000;
app.listen(port, ()=> {
    console.log(`App is running in the ${port}. done`);
})